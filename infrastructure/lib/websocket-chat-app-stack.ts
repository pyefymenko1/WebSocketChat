import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as dynamodb from 'aws-cdk-lib/aws-dynamodb';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as apigwv2 from '@aws-cdk/aws-apigatewayv2-alpha';
import * as apiGwV2Int from '@aws-cdk/aws-apigatewayv2-integrations-alpha';
import path = require('path');

export class WebsocketChatAppStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    
    const connectionMappingTable = new dynamodb.Table(this, 'ConnectionMappingTable', {
      tableName: 'websocket-chat-connection-mapping',
      partitionKey: { name: 'connectionId', type: dynamodb.AttributeType.STRING },
      billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
      encryption: dynamodb.TableEncryption.AWS_MANAGED,
      removalPolicy: cdk.RemovalPolicy.DESTROY
    });

    const onConnectFunction = new lambda.Function(this, 'OnConnectFunction', {
      functionName: 'websocket-chat-connect',
      runtime: lambda.Runtime.DOTNET_6,
      handler: 'WebSocketChat::WebSocketChat.Functions::OnConnectHandler',
      code: lambda.Code.fromAsset(path.join(__dirname, 'lambda-handler', 'WebSocketChat.zip')),
      timeout: cdk.Duration.seconds(30),
      environment: {
        TABLE_NAME: connectionMappingTable.tableName
      }
    });

    const onDisconnectFunction = new lambda.Function(this, 'OnDisconnectFunction', {
      functionName: 'websocket-chat-disconnect',
      runtime: lambda.Runtime.DOTNET_6,
      handler: 'WebSocketChat::WebSocketChat.Functions::OnDisconnectHandler',
      code: lambda.Code.fromAsset(path.join(__dirname, 'lambda-handler', 'WebSocketChat.zip')),
      timeout: cdk.Duration.seconds(30),
      environment: {
        TABLE_NAME: connectionMappingTable.tableName
      }
    });

    const sendMessageFunction = new lambda.Function(this, 'SendMessageFunction', {
      functionName: 'websocket-chat-sendmessage',
      runtime: lambda.Runtime.DOTNET_6,
      handler: 'WebSocketChat::WebSocketChat.Functions::SendMessageHandler',
      code: lambda.Code.fromAsset(path.join(__dirname, 'lambda-handler', 'WebSocketChat.zip')),
      timeout: cdk.Duration.seconds(30),
      environment: {
        TABLE_NAME: connectionMappingTable.tableName
      }
    });

    connectionMappingTable.grantReadWriteData(onConnectFunction);
    connectionMappingTable.grantReadWriteData(onDisconnectFunction);
    connectionMappingTable.grantReadWriteData(sendMessageFunction);
    
    const webSocketApi = new apigwv2.WebSocketApi(this, 'WebSocketApi');
    
    const webSocketStage = new apigwv2.WebSocketStage(this, 'WebSocketStage', {
      webSocketApi,
      stageName: 'prod',
      autoDeploy: true,
    });
    
    webSocketApi.addRoute('sendmessage', {
      integration: new apiGwV2Int.WebSocketLambdaIntegration('SendMessage', sendMessageFunction),
    });

    webSocketApi.addRoute('$connect', {
      integration: new apiGwV2Int.WebSocketLambdaIntegration('Connect', onConnectFunction),
    });

    webSocketApi.addRoute('$disconnect', {
      integration: new apiGwV2Int.WebSocketLambdaIntegration('Disconnect', onDisconnectFunction),
    });

    webSocketApi.grantManageConnections(sendMessageFunction);

    new cdk.CfnOutput(this, 'WebSocketUrl', {
      value: `wss://${webSocketApi.apiId}.execute-api.${this.region}.amazonaws.com/${webSocketStage.stageName}`,
    });
  }
}
